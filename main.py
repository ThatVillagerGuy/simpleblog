import glob
import os
import datetime
import math
import shutil

 

def main():
    if os.path.exists("./pages"):
        shutil.rmtree("./pages")
    os.makedirs("./pages")
    os.makedirs("./pages/posts")
    files = glob.glob("./posts/*")
    global pages
    pages = math.ceil(len(files)/10)
    print(pages)
    for i in range(1, pages):
        newPage(files[(i-1)*10: i*10],"./pages/page_%s.html"%i, i)
    newPage(files[(pages-1)*10:], "./pages/page_%s.html"%pages, pages)

def newPage(files, path, page):
    base = open("./base/index.html")
    new_file = open(path, "a")
   
    for i in base:
        if "style.css"in i :
            new_file.write('<link rel="stylesheet" type="text/css" href="../base/style.css">')
        elif '<div id="content">' in i:
            new_file.write(i)
            for arg in files:
                if  arg!= "main.py":
                    print("Adding " + arg)
                    content_file = open(arg)
                    
                    #Single pages for posts
                    new_post = open("./pages/{}".format(arg), "a")
                    tmp_base = open("./base/index.html")
                    for j in tmp_base:
                        if "style.css" in j:
                            new_post.write('<link rel="stylesheet" type="text/css" href="/base/style.css">')
                        elif '<div id="content">' in j:
                            new_post.write(j)
                            for e in content_file:
                                new_post.write(e)
                        else:
                            new_post.write(j)
                    new_post.close()
                    tmp_base.close()
                    content_file.close()
                    content_file = open(arg)
                    new_file.write("<a class='ln' href='{}'>[link]</a>".format(arg))
                    for c in content_file:
                        if '</p>' not in c:
                            new_file.write(c)
                        else:
                             break
                    new_file.write("<br>")
                    new_file.write("created on: %s " % datetime.datetime.fromtimestamp(os.path.getctime(arg)).strftime("%m/%d/%Y, %H:%M:%S"))
                    if os.path.getctime(arg) >= os.path.getmtime(arg)+15*60:
                        new_file.write("<br>last modified on: %s <br> (updated)" % datetime.datetime.fromtimestamp(os.path.getmtime(arg)).strftime("%m/%d/%Y, %H:%M:%S"))
                    new_file.write("<hr><br>")
                    
        elif '<footer>' in i:
            new_file.write(i)
            if page < pages and page > 1:
                new_file.write("<p class ='pages'><a href='{}'>&lt;</a>Page {}/{}<a href='{}'>&gt;</a></p>".format("./pages/page_{}.html".format(page-1), page, pages, "./pages/page_{}.html".format(page+1)))
            elif page < pages:
                new_file.write("<p class ='pages'><span style='color: grey'>&lt;</span>Page {}/{}<a href='{}'>&gt;</a></p>".format(page, pages, "./pages/page_{}.html".format(page+1)))
            elif page > 1:
                new_file.write("<p class ='pages'><a href='{}'>&lt;</a>Page {}/{}<span style='color: grey'>&gt;</span></p>".format("./pages/page_{}.html".format(page-1), page, pages))


                    
        else:
            new_file.write(i)


    content_file.close()
    base.close()
    new_file.close()

    

def toHTML(input):
    print("This does not work yet")

if __name__ == "__main__":
    main()
